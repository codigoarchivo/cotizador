import React, { Component } from 'react';
class Formulario extends Component {
    state = {
        marca: '',
        anio: '',
        plan: ''
    }
    enviarInformacion = (e) => {
        e.preventDefault();
        //*parte de los props
        let { marca, anio, plan } = this.state;
        this.props.enviarCalculo(marca, anio, plan);
    }
    agregarInformacio = (e) => {
        let { name, value } = e.target,
            unir = [];
        //*parte de el cambio: String o Number
        unir.push(name === "plan" ? String(value) : Number(value));
        //*parte guardar en el states
        this.setState({
            [name]: unir[0]
        });
    }
    validaciones = () => {
        //*parte de las validaciones
        let { marca, anio, plan } = this.state,
            validar = !marca || !anio || !plan
        return validar
    }
    render() {
        //*parte de options
        let max = new Date().getFullYear(),
            min = max - 20,
            state = new Map(),
            Options = [];
        for (let i = max; min <= i; --i) { state.set(i, i) }
        state.forEach((clave, valor) => Options.push(
            <option key={clave} value={clave}>{valor}</option>));
        return (
            <form onSubmit={this.enviarInformacion} className='form'>
                <fieldset className='form__fieldset'>
                    <legend className='form__legend'>Cotizador</legend>

                    <div className='form__box'>
                        <label className='form__label'>Marca</label>
                        <select onChange={this.agregarInformacio}
                            className='form__select'
                            name='marca'
                            id='marca'>
                            <option value=''>Selecciona</option>
                            <option value="1">Americano</option>
                            <option value="2">Asiatico</option>
                            <option value="3">Europeo</option>
                        </select>
                    </div>

                    <div className='form__box'>
                        <label className='form__label'>Año</label>
                        <select onChange={this.agregarInformacio}
                            className='form__select'
                            name='anio'
                            id='anio'>
                            <option value=''>Selecciona</option>
                            {Options}
                        </select>
                    </div>

                    <div className='form__box'>
                        <h1 className='form__h1'>Tipo de Seguro</h1>
                        <div className='form__cont'>
                            <input onChange={this.agregarInformacio}
                                className="form__input"
                                id="basico"
                                type="radio"
                                value="Básico"
                                name="plan" />
                            <label htmlFor="basico" className='form__label--seguro'>
                                <span className="form__radio--bottom"></span>Básico</label>
                        </div>

                        <div className='form__cont'>
                            <input onChange={this.agregarInformacio}
                                className="form__input"
                                id="completo"
                                type="radio"
                                value="Completo"
                                name="plan" />
                            <label htmlFor="completo" className='form__label--seguro'>
                                <span className="form__radio--bottom"></span>Completo</label>
                        </div>
                    </div>

                    <div id="resultado"></div>
                    <div className="form__box">
                        <input disabled={this.validaciones()}
                            className='btn btn__color'
                            value='Calcular'
                            type='submit' />
                    </div>
                </fieldset>
            </form>
        );
    }
}

export default Formulario;