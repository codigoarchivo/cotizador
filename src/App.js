import React, { Component, Fragment } from 'react';
import Header from './layouts/Header';
import Footer from './layouts/Footer';
import Formulario from './components/Formulario';
import { configCotizar } from './Helper';
import Resultado from './components/Resultados';
import Spinner from './components/Spinner';
class App extends Component {
  state = {
    marca: '',
    anio: '',
    plan: '',
    cotizar: '',
    validar: true
  }
  //*parte de los props y guardar states
  enviarCalculo = (marca, anio, plan) => {
    let cotizar = configCotizar(marca, anio, plan);
    this.setState({
      validar: false
    }, () => {
      //*parte despues 3000ms sale el resultado en el states
      setTimeout(() => {
        this.setState({
          marca,
          anio,
          plan,
          cotizar,
          validar: true
        });
      }, 3000);
    });
  }
  render() {
    let { marca, anio, plan, cotizar, validar } = this.state;
    let components;
    //*parte de si cotizar esta vacio y si validar es false
    if (cotizar === '' && validar)
      components = <Resultado
        marca={marca}
        anio={anio}
        plan={plan}
        cotizar={cotizar}
      />;
    //*parte de si validar es true: spinner
    else if (!validar)
      components = <Spinner />
    //*parte de si validar es false: cotizado
    else
      components = <Resultado
        marca={marca}
        anio={anio}
        plan={plan}
        cotizar={cotizar}
      />;

    return (
      <Fragment>
        <Header />
        <Formulario
          enviarCalculo={this.enviarCalculo}
        />
        {components}
        <Footer />
      </Fragment>
    );
  }
}

export default App;