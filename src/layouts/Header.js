import React from 'react';
import '../index.css'
const Header = () => {
    return (
        <header className='header'>
            <div className='header__box'>
                <h1 className='header__box--h1'>Cotiza Tu Auto</h1>
                <sub className='header__box--p'>Lorem Ipsum es simplemente <b><big>CódigoArchivo</big></b></sub>
            </div>
        </header>
    );
}

export default Header;