import React from 'react';
const Footer = () => {
    return (
        <footer className='footer'>
            <p className='footer__p'>Todos Los Derechos Reservados CódigoArchivo Copyright &copy; {new Date().getFullYear()}</p>
        </footer>
    );
}

export default Footer;