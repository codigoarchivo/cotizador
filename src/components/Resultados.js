import React from 'react';
import { modificarMarca } from '../Helper';
const Resultado = (props) => {
    //*parte de los props
    let { marca, anio, plan, cotizar } = props,
        //*parte de modificar la marca de numero a  contenido
        Marca = modificarMarca(marca);
    return (
        <section className='resultado'>
            <ul className='resul__ul'>
                <li className='resul__li'>
                    <b className='resul__p-b'>Marca:</b>
                    <span className='resul__p--span'>{Marca}</span>
                </li>
                <li className='resul__li'>
                    <b className='resul__p-b'>Año:</b>
                    <span className='resul__p--span'>{anio}</span>
                </li>
                <li className='resul__li'>
                    <b className='resul__p-b'>Plan:</b>
                    <span className='resul__p--span'>{plan}</span>
                </li>
                <li className='resul__li'>
                    <b className='resul__p-b'>Cotizar:</b>
                    <span className='resul__p--span'>$ {cotizar}</span>
                </li>
            </ul>
        </section>
    );
}

export default Resultado;