//*Cotizador
export function configCotizar(marca, anio, plan) {
    let base = 2000;
    switch (marca) {
        case 1:
            base *= 1.15;
            break;
        case 2:
            base *= 1.05;
            break;
        case 3:
            base *= 1.35;
            break;
        default:
            break;
    }
    let calular = new Date().getFullYear() - anio;
    base -= ((calular * 3) * base) / 100;
    switch (plan) {
        case "basico":
            base *= 1.30;
            break;
        case "completo":
            base *= 1.50;
            break;
        default:
            break;
    }
    return Number(base.toFixed(2));
}
//*Cambio a letra
export function modificarMarca(marca) {
    let marcas;
    switch (marca) {
        case 1:
            marcas = 'Americano';
            break;
        case 2:
            marcas = 'Asiatico';
            break;
        case 3:
            marcas = 'Europeo';
            break;
        default:
            break;
    }
    return marcas
}